package com.addcel.telecom.wallet.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.wallet.client.model.vo.EstadosResponse;
import com.addcel.telecom.wallet.client.model.vo.TelecomCard;
import com.addcel.telecom.wallet.client.model.vo.TarjetaRequest;
import com.addcel.telecom.wallet.client.model.vo.TarjetaRequestTC;
import com.addcel.telecom.wallet.client.model.vo.TarjetaResponse;
import com.addcel.telecom.wallet.mybatis.model.vo.Estados;
import com.addcel.telecom.wallet.services.ServicesCard;
import com.addcel.telecom.wallet.services.UtilsService;
import com.addcel.utils.AddcelCrypto;



@Controller
public class RestServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServices.class);
	
	@Autowired
	private ServicesCard servicesCard;
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public ModelAndView getToken() {
		System.out.println(UtilsService.getSMS("SjfPOM4e6MBIEG1Z4h6Ecg=="));
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}

	@RequestMapping(value = "telecom/delete", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> delete(@RequestParam("idTarjeta") int idTarjeta , @RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.delete(idTarjeta, idUsuario,idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/update", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> update(@RequestBody  TarjetaRequest cardRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.update(cardRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/getTarjetas", method=RequestMethod.GET,produces = "application/json")	
	public ResponseEntity<TarjetaResponse> get(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		
		//servicesCard.CreateCardFV("MAriana", "Lopez", "Arapahoe Av. 41", "CO", "Boulder", "019283", "hola@gamil.com", "3344332211", "04071988", "111111122", 123456531);
		LOGGER.info("telecom/getTarjetas");
		return  new ResponseEntity<TarjetaResponse>(servicesCard.getCards(idUsuario, idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/add", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> Add(@RequestBody  TarjetaRequest cardRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.addCard(cardRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/addTelecom", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> addCardTelecom(@RequestBody TarjetaRequestTC carRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.AddTelecomCard(carRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/{idpais}/estados", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<EstadosResponse> getEstados(@PathVariable int idpais) {
		return  new ResponseEntity<EstadosResponse>(servicesCard.getEstates(idpais),HttpStatus.OK);
	}
	
	/*PRUEBAS DE INGO*/
	@RequestMapping(value = "telecom/getCardIngo", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getCardIngo(@RequestParam("deviceId") String deviceId,@RequestParam("customerId") String customerId) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.GetCardI(deviceId,customerId),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/getFindCustomer", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getFindCustomer(@RequestParam("deviceId") String deviceId,@RequestParam("dateOfBirth") String dateOfBirth,@RequestParam("ssn") String ssn) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.findCustomerI(deviceId,dateOfBirth,ssn),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/getSessionLoad", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getCardIngo(@RequestParam("deviceId") String deviceId,@RequestParam("user") String user,@RequestParam("password") String password) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.GetSessionLoadI(deviceId,user,password),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/enrollCustomer", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> enrollCustomer(@RequestParam("deviceId") String deviceId,@RequestParam("address") String address,@RequestParam("city") String city,@RequestParam("dateOfBirth") String dateOfBirth,@RequestParam("email") String email,@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,@RequestParam("mobileNumber") String mobileNumber,@RequestParam("ssn") String ssn,@RequestParam("state") String state,@RequestParam("zip") String zip) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.enrollCustomerI(address, city, dateOfBirth, email, firstName, lastName, mobileNumber, ssn, state, zip, deviceId),HttpStatus.OK);
	}
	
	@RequestMapping(value = "telecom/AddOrUpdateCard", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> enrollCustomer(@RequestParam("address") String address,@RequestParam("cardNickname") String cardNickname,@RequestParam("cardNumber") String cardNumber,@RequestParam("city") String city,@RequestParam("custumerId") String custumerId,@RequestParam("deviceId") String deviceId,@RequestParam("expirationMonthYear") String expirationMonthYear,@RequestParam("nameOncard") String nameOncard,@RequestParam("state") String state,@RequestParam("zip") String zip) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.addCardIngoI(address,cardNickname,cardNumber,city,custumerId,deviceId,expirationMonthYear,nameOncard,state,zip),HttpStatus.OK);
																				      
	}
	
	
	@RequestMapping(value = "telecom/IngoNotifications", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<TarjetaResponse> IngoNotifications(@RequestParam("customerId") String customerId,@RequestParam("deviceId") String deviceId,@RequestParam("customerEmailAddress") String customerEmailAddress,@RequestParam("firstName") String firstName,@RequestParam("cancelledByDescription") String cancelledByDescription,@RequestParam("cardBinNumber") String cardBinNumber,@RequestParam("cardId") String cardId,@RequestParam("cardNickName") String cardNickName,@RequestParam("checkSubmissionDate") String checkSubmissionDate,@RequestParam("declineCode") String declineCode,@RequestParam("declineReason") String declineReason,@RequestParam("fundingDestinationIdentifier") String fundingDestinationIdentifier,@RequestParam("fundsAvailableDate") String fundsAvailableDate,@RequestParam("lastFourOfCard") String lastFourOfCard,@RequestParam("lastName") String lastName,@RequestParam("notificationId") String notificationId,@RequestParam("notificationTypeDescription") String notificationTypeDescription,@RequestParam("promoCode") String promoCode,@RequestParam("promoType") String promoType,@RequestParam("transactionId") String transactionId,@RequestParam("transactionStatusDescription") String transactionStatusDescription,@RequestParam("transactionTypeDescription") String transactionTypeDescription,@RequestParam("cancelledBy") int cancelledBy,@RequestParam("cancelledBy") Long fee,@RequestParam("feeDeltaInCents") Long feeDeltaInCents,@RequestParam("isPpg") boolean isPpg,@RequestParam("keyedAmount") Long keyedAmount,@RequestParam("loadAmount") Long loadAmount,@RequestParam("notificationType") Long notificationType,@RequestParam("promoAmount") Long promoAmount,@RequestParam("transactionStatus") int transactionStatus,@RequestParam("transactionType") int transactionType) {		
//	public ResponseEntity<TarjetaResponse> IngoNotifications(@RequestParam("customerId") String customerId,@RequestParam("deviceId") String deviceId,@RequestParam("customerEmailAddress") String customerEmailAddress,@RequestParam("firstName") String firstName,@RequestParam("cancelledByDescription") String cancelledByDescription,@RequestParam("cardBinNumber") String cardBinNumber,@RequestParam("cardId") String cardId,@RequestParam("cardNickName") String cardNickName,@RequestParam("checkSubmissionDate") String checkSubmissionDate,@RequestParam("declineCode") String declineCode,@RequestParam("declineReason") String declineReason,@RequestParam("fundingDestinationIdentifier") String fundingDestinationIdentifier,@RequestParam("fundsAvailableDate") String fundsAvailableDate,@RequestParam("lastFourOfCard") String lastFourOfCard,@RequestParam("lastName") String lastName,@RequestParam("notificationId") String notificationId,@RequestParam("notificationTypeDescription") String notificationTypeDescription,@RequestParam("promoCode") String promoCode,@RequestParam("promoType") String promoType,@RequestParam("transactionId") String transactionId,@RequestParam("transactionStatusDescription") String transactionStatusDescription,@RequestParam("transactionTypeDescription") String transactionTypeDescription,@RequestParam("cancelledBy") int cancelledBy,@RequestParam("fee") Long fee) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.ingoNotificationsI(deviceId,customerId,customerEmailAddress,firstName,cancelledByDescription,cardBinNumber,cardId,cardNickName,checkSubmissionDate,declineCode,declineReason,fundingDestinationIdentifier,fundsAvailableDate,lastFourOfCard,lastName,notificationId,notificationTypeDescription,promoCode,promoType,transactionId,transactionStatusDescription,transactionTypeDescription,cancelledBy,fee,feeDeltaInCents,isPpg,keyedAmount,loadAmount,notificationType,promoAmount,transactionStatus,transactionType),HttpStatus.OK);		
		
		
																				      
	}
	
	
	
	/*FIN DE LA PRUEBA*/
	
	
}
