package com.addcel.telecom.wallet.client.model.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TarjetaResponse extends McResponse {

	@SerializedName("TelecomCardCard")
	@Expose
	private List<TelecomCard>	tarjetas;

	public TarjetaResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public List<TelecomCard> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<TelecomCard> tarjetas) {
		this.tarjetas = tarjetas;
	}
	
	
}
