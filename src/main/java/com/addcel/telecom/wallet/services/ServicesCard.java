package com.addcel.telecom.wallet.services;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import crypto.Crypto;

import com.addcel.telecom.firstView.service.ServicesProxy;
import com.addcel.telecom.firstView.service.model.CreateCard;
import com.addcel.telecom.firstView.service.model.CreateCardResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.IngoNotifications;
import com.addcel.telecom.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.telecom.wallet.client.viamericas.ViamericasClient;
import com.addcel.telecom.wallet.client.model.vo.EstadosResponse;
import com.addcel.telecom.wallet.client.model.vo.McResponse;
import com.addcel.telecom.wallet.client.model.vo.TelecomCard;
import com.addcel.telecom.wallet.client.viamericas.ProfileCardRequest;
import com.addcel.telecom.wallet.client.model.vo.TarjetaRequest;
import com.addcel.telecom.wallet.client.model.vo.TarjetaRequestTC;
import com.addcel.telecom.wallet.client.model.vo.TarjetaResponse;
import com.addcel.telecom.wallet.mybatis.model.mapper.ServiceMapper;
import com.addcel.telecom.wallet.mybatis.model.vo.Card;
import com.addcel.telecom.wallet.mybatis.model.vo.Estados;
import com.addcel.telecom.wallet.mybatis.model.vo.Login;
import com.addcel.telecom.wallet.mybatis.model.vo.User;
import com.addcel.telecom.wallet.utils.Constants;
import com.addcel.utils.AddcelCrypto;


@Service
public class ServicesCard {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServicesCard.class);
	
	@Autowired
	private ServiceMapper mapper;
	
	@Autowired
	private MCIngorequest mcIngorequest;
	
	@Autowired
	ViamericasClient viamericasClient;
	
	private Gson gson = new Gson();	
	
	
	
	public TarjetaResponse addCard(TarjetaRequest cardRequest){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE INSERCION DE TARJETA {} ", cardRequest);
			Login login = mapper.getLogin(cardRequest.getIdUsuario()); 
			String pan =  AddcelCrypto.decryptHard(cardRequest.getPan());
			String vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
			
			String cod = cardRequest.getCodigo() == null ? "" : AddcelCrypto.decryptHard(cardRequest.getCodigo());
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", pan);
			parameters.put("nnumtarjetacrip", UtilsService.setSMS(pan));
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 1);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex() );
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("telecomcard", cardRequest.isTelecomcard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
						
			
			LOGGER.debug("parameters add card: " + parameters.get("nproceso") );
			
			String responseAdd = mapper.ProcessCard(parameters);
			McResponse add_response = gson.fromJson(responseAdd, McResponse.class);
			
			LOGGER.debug("Respuesta add: " + responseAdd);	
			parameters.replace("nproceso", 3);
			
			response = getCards(parameters);
			response.setIdError(add_response.getIdError());
			response.setMensajeError(add_response.getMensajeError());
			
			//VIAMERICAS SOLO SI ES USA
			if(login.getIdpais() == 3)
				CreateProfileViamericas(cardRequest.getDomAmex(), cardRequest.getNombre().split(" ")[0], cardRequest.getNombre().split(" ")[1], pan, cod, vig.replace("/", ""), login.getId_sender(), cardRequest.getIdUsuario(), login.getUser().split("@")[0], "K");
			
			
			LOGGER.debug("FINALIZANDO PROCESO DE INSERCION DE TRAJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir tarjeta : " + ex.getMessage());
			LOGGER.error("Error:", ex);
			return LogService.getAddMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}	 
	
	private void CreateProfileViamericas(String address, String cardFirstName, String cardLastName, String cardNumber, String cvv, String expDate, String idSender, long idUsuario, String nickName, String paymentType){
		try{
			if(idSender != null && !idSender.isEmpty())
			{
				ProfileCardRequest request = new ProfileCardRequest();
				request.setAddress(address);
				request.setCardFirstName(cardFirstName);
				request.setCardLastName(cardLastName);
				request.setCardNumber(cardNumber);
				request.setCvv(cvv);
				request.setExpDate(expDate);
				request.setIdSender(idSender);
				request.setIdUsuario(idUsuario);
				request.setNickName(nickName);
				request.setPaymentType(paymentType);
			
				viamericasClient.createSenderProfileCard(request);
			}
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL CREAR PROFILE VIAMERICAS]", ex);
		}
	}
	public TarjetaResponse update(TarjetaRequest cardRequest){
		TarjetaResponse response= new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE ACTUALIZACION");
			Login login = mapper.getLogin(cardRequest.getIdUsuario());
			HashMap parameters = new HashMap<>();
			
			String vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
			String cod =  AddcelCrypto.decryptHard(cardRequest.getCodigo());
			
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 5);
			parameters.put("nidtarjetausuario", cardRequest.getIdTarjeta());
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex());
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("telecomcard", cardRequest.isTelecomcard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
			
			LOGGER.info("login.getUser()"+login.getUser());
			LOGGER.info("login.getPass()"+login.getPass());
			LOGGER.info("getIdTarjeta "+cardRequest.getIdTarjeta());
			LOGGER.info("cardRequest.getDomAmex()"+cardRequest.getDomAmex());
			//LOGGER.debug("IsMobileCard: " + cardRequest.isMobilecard());
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("Campos actualizados... " + responseJson);
			
			McResponse mcResponseUpdate = gson.fromJson(responseJson, McResponse.class);
			
			
			if(mcResponseUpdate.getIdError() == 0 && cardRequest.isDeterminada())
			{
				parameters.replace("nproceso", 2);
				McResponse mcResponse = active(parameters);
				LOGGER.debug("activando");
				if(mcResponse.getIdError()!= 0){
					response.setIdError(mcResponse.getIdError());
					response.setMensajeError(mcResponse.getMensajeError());
				}
				
			}
			
			parameters.put("nidtarjetausuario", 1); ///para saber si es de mexico la peticion
			parameters.replace("nproceso", 3);
			LOGGER.debug("cards obtenidas...");
		//	responseJson = getCards(parameters);
			response = getCards(parameters);//gson.fromJson(responseJson,TarjetaResponse.class);
			response.setIdError(mcResponseUpdate.getIdError());
			response.setMensajeError(mcResponseUpdate.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE ACTUALIZACION");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir actualizar tarjeta : " + ex.getMessage());
			response.setIdError(101);
			response.setMensajeError("error al agregar actualizar tarjeta");
			return LogService.CardUpdateMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}
	
	
	
	
	
	public TarjetaResponse getCards(long idUsuario, String idioma){
		try{
			LOGGER.debug("INICIANDO PROCESO GETCARDS");
			Login login = mapper.getLogin(idUsuario); 
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 3);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("telecomcard", 0);
			parameters.put("tipotarjeta", "");
			LOGGER.debug("fINALIZANDO PROCESO GETCARDS");
			return getCards(parameters);
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : " + ex.getMessage());
			return LogService.getCardsMessageerror(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	
	public TarjetaResponse delete(int idTarjeta, long idUsuario, String idioma){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE BORRADO DE TARJETA");
			Login login = mapper.getLogin(idUsuario); 
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 4);
			parameters.put("nidtarjetausuario", idTarjeta);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("telecomcard", 0);
			parameters.put("tipotarjeta", "");
			TarjetaResponse responseDelete = delete(parameters);
			
			parameters.replace("nproceso", 3);
			parameters.replace("nidtarjetausuario", 1);
			
			response = getCards(parameters);
			
			response.setIdError(responseDelete.getIdError());
			response.setMensajeError(responseDelete.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE BORRADO DE TARJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : " + ex.getMessage());
			
			return LogService.CardDeleteMessageError(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public EstadosResponse getEstates(int idpais)
	{
		List<Estados> estados = mapper.getEstates(idpais);
		
		EstadosResponse response = new EstadosResponse();
		response.setEstados(estados);
		
		
		if(estados != null && estados.size() > 0)
		{
			response.setIdError(0);
			response.setMensajeError("");
		}
		else
		{
			response.setIdError(-1);
			if(idpais == 1)
				response.setMensajeError("País sin estados en catálogo");
			else
				response.setMensajeError("Country without states in catalog");
		}
		
		
		LOGGER.debug("Estados Count: " + estados.size());
		
		return response;
	}
	
	public TarjetaResponse AddTelecomCard(TarjetaRequestTC carRequest){
		
		LOGGER.debug("AddTelecomCard !!! TelecomGestionWallet");
		TarjetaResponse response =  new TarjetaResponse();
		CustomerResponse responseIngo = new CustomerResponse();
		User user = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); 
		String expira = "";
		String anio = "";
		String mes = "";
		Card telecomcardCard = null;
		CreateCardResponse responseFV = new CreateCardResponse();
		GetRegisteredCardsRequestMC resgisterCards = new GetRegisteredCardsRequestMC();
		int cards = 0;
		
		try{
			user= mapper.getUser(carRequest.getIdUsuario());
			user.setUsr_direccion(carRequest.getDireccion());
			user.setUsr_ciudad(carRequest.getCiudad());
			user.setUsr_cp(carRequest.getCp());
			user.setUsr_fecha_nac(new java.sql.Date(sdf.parse(carRequest.getFechaNac()).getTime()));
			user.setUsr_nss(carRequest.getSsn());
			
			//enrrolar usuario en ingo
			if(user.getId_ingo() == null || user.getId_ingo().isEmpty() || user.getId_ingo().equals("6ff553f0-015a-416a-98cd-6232bd979b62")){	
				responseIngo = EnrollCustomer(carRequest.getDireccion(), carRequest.getCiudad(),carRequest.getFechaNac().substring(4,8)+"-"+carRequest.getFechaNac().substring(2,4)+"-"+carRequest.getFechaNac().substring(0,2),user.geteMail(), user.getUsr_nombre(), user.getUsr_apellido(), user.getUsr_telefono(), carRequest.getSsn(), carRequest.getEstado(),carRequest.getCp(),user.getImei());
				user.setId_ingo(responseIngo.getCustomerId());
				LOGGER.debug("Usuario con id_ingo: "+responseIngo.getCustomerId());
				mapper.updateCustomerId(user);
			}
			else
			{
				LOGGER.debug("Usuario ya se encuentra en INGO... [RecuPerando Datos]");
				responseIngo.setCustomerId(user.getId_ingo());
				telecomcardCard = mapper.getTelecomcardCard(user.getId_usuario());
										 
			}
			
			 
			//Checar si tiene tajetas en ingo
			resgisterCards.setCustomerId(responseIngo.getCustomerId());
			resgisterCards.setDeviceId(user.getImei());
			//CardsResponse cards_response =  mcIngorequest.GetRegisteredCards(resgisterCards, resgisterCards.getDeviceId());
			cards = 0;//cards_response.getCards().size();
			//validando que no tenga registrada tarjeta MC en ingo
			if(cards == 0 )
			{
					//crear tarjeta FV
					if(telecomcardCard == null || telecomcardCard.getPan() == null ){
						
						LOGGER.info("user.getUsr_nombre(): "+user.getUsr_nombre());
						LOGGER.info("user.getUsr_apellido(): "+user.getUsr_apellido());
						LOGGER.info("carRequest.getDireccion(): "+carRequest.getDireccion());
						LOGGER.info("carRequest.getEstado(): "+carRequest.getEstado());
						LOGGER.info("carRequest.getCiudad(): "+carRequest.getCiudad());
						LOGGER.info("carRequest.getCp(): "+carRequest.getCp());
						LOGGER.info("user.geteMail(): "+user.geteMail());
						LOGGER.info("user.getUsr_telefono(): "+user.getUsr_telefono());
						LOGGER.info("carRequest.getFechaNac(): "+carRequest.getFechaNac());
						LOGGER.info("carRequest.getSsn(): "+carRequest.getSsn());
						LOGGER.info("carRequest.getIdUsuario(): "+carRequest.getIdUsuario());
						LOGGER.info("carRequest.getGovtId(): "+carRequest.getGovtId());
						LOGGER.info("carRequest.getGovtIdExpirationDate(): "+carRequest.getGovtIdExpirationDate());
						LOGGER.info("carRequest.getGovtIdIssueDate(): "+carRequest.getGovtIdIssueDate());
						LOGGER.info("carRequest.getGovtIdIssueState(): "+carRequest.getGovtIdIssueState());				
						
						
						responseFV = CreateCardFV(user.getUsr_nombre(),user.getUsr_apellido(),carRequest.getDireccion(),carRequest.getEstado(),carRequest.getCiudad(),carRequest.getCp(), user.geteMail(),user.getUsr_telefono(),carRequest.getFechaNac().substring(2,4) + carRequest.getFechaNac().substring(0,2) + carRequest.getFechaNac().substring(4,carRequest.getFechaNac().length()),carRequest.getSsn(),carRequest.getIdUsuario(),carRequest.getGovtId(), carRequest.getGovtIdExpirationDate(),carRequest.getGovtIdIssueDate(),carRequest.getGovtIdIssueState());			
						//lo comento hasta q deploye FirstView
						LOGGER.info("telecomcardCard == null!!!!!!!!!!!! V2222222222");
////						responseFV.setCardNumber(UtilsService.getSMS("5518307714"));
//						responseFV.setCardNumber("5164850000053016");
////						responseFV.setCvv(UtilsService.getSMS("123"));
//						responseFV.setCvv("123");
//						responseFV.setErrorCode(0);
//						responseFV.setErrorDescription("");
////						responseFV.setExpirationDate(UtilsService.getSMS("0321"));
//						responseFV.setExpirationDate("0421");						
//						responseFV.setIdUsuario(user.getId_usuario());
						
					}else
					{
						//Recuperar datos de tarjeta del wallet
						responseFV.setCardNumber(UtilsService.getSMS(telecomcardCard.getPan()));
						responseFV.setCvv(UtilsService.getSMS(telecomcardCard.getCodigo()));
						responseFV.setErrorCode(0);
						responseFV.setErrorDescription("");
						responseFV.setExpirationDate(UtilsService.getSMS(telecomcardCard.getVigencia()));
						responseFV.setIdUsuario(user.getId_usuario());
						
					}
					
					if(responseFV.getErrorCode() == 0 && responseFV.getCardNumber()!=null && responseFV.getCvv()!= null && responseFV.getExpirationDate() != null){
						
						 mes = responseFV.getExpirationDate().substring(0,2);
						 anio = responseFV.getExpirationDate().substring(responseFV.getExpirationDate().length()-2);
						 expira = mes+anio; //MMYY
						
						if(responseIngo.getCustomerId() != null && !responseIngo.getCustomerId().isEmpty())
						{
							//agregar targeta a Ingo
							resgisterCards.setCustomerId(responseIngo.getCustomerId());
							resgisterCards.setDeviceId(user.getImei());
							
							//
					        CardsResponse responseCardIngo = addCardIngo(user.getUsr_direccion(), user.getUsr_nombre(), responseFV.getCardNumber(), carRequest.getCiudad(), responseIngo.getCustomerId(), user.getImei(),expira , (user.getUsr_nombre() + " " + user.getUsr_apellido()).trim() , carRequest.getEstado(), user.getUsr_cp());
					        
					        if(responseCardIngo.getErrorCode() == 0){
								LOGGER.debug("Se Agrego correctamente la tarjeta al usuario {}",responseIngo.getCustomerId() );
							}
							else
							{
								LOGGER.debug("Error al agregar tarjeta al usuario {} message {} ", responseIngo.getCustomerId(), responseCardIngo.getErrorMessage());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
					        
						}else
						{
							LOGGER.debug("Error al enrrolar usuario en ingo {} {}", carRequest.getIdUsuario(), responseIngo.getErrorMessage());
							response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
						}
						
						
						//agregar Tarjeta al wallet
						if(telecomcardCard == null || telecomcardCard.getPan() == null)
						{
							TarjetaRequest cardRequest = new TarjetaRequest();
							//cardRequest.setCodigo(responseFV.getCvv());
							cardRequest.setCpAmex("");
							cardRequest.setDeterminada(false);
							cardRequest.setDomAmex("");
							cardRequest.setIdioma(carRequest.getIdioma());
							cardRequest.setIdTarjeta(0);
							cardRequest.setIdUsuario(user.getId_usuario());
							cardRequest.setTelecomcard(true);
							cardRequest.setNombre(user.getUsr_nombre().toString() + " " + user.getUsr_apellido().trim());
							cardRequest.setPan(AddcelCrypto.encryptHard(responseFV.getCardNumber()));
							cardRequest.setTipo(0);
							cardRequest.setVigencia(AddcelCrypto.encryptHard(mes+"/"+anio));
							cardRequest.setCodigo(AddcelCrypto.encryptHard(responseFV.getCvv()));
							cardRequest.setTipoTarjeta(Constants.CREDIT_CARD_TYPE);
							
							response = addCard(cardRequest);
							
							if(response.getIdError() == 0){
								LOGGER.debug("Se agrego la Tarjeta correctamente al Wallet");
							}
							else{
								LOGGER.debug("Error al agregar tarejta al wallet {} {}", response.getMensajeError(), cardRequest.getIdUsuario());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
						}else
							LOGGER.debug("LA TARJETA YA SE ENCONTRABA EN EL WALLET...");
						
						
					}else{
						LOGGER.debug("Error al crear Targeta FirstView {} {} " + carRequest.getIdUsuario(), responseFV.getErrorDescription());
						response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2); //LogService.ErrorFV(carRequest.getIdioma().equals("es") ? 1 : 2);
					}
			}
			else
			{
				LOGGER.debug("Se detecto un error con una tarjeta registrada previamente, contacte al administrador");
				response.setIdError(110);
				response.setMensajeError("An error was detected with a previously registered card, contact technical support"); //Se detecto un error con una tarjeta registrada previamente, contacte a soporte tecnico
				response.setTarjetas(new ArrayList<TelecomCard>());
			}
			
		}catch(Exception ex){
			LOGGER.error("Error al crear tarjetaTelecom", ex);
			
			return LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
		}
		return response;
		
	}
	
	/*public TarjetaResponse AddMobileCardCard(TarjetaRequestMC carRequest){
		TarjetaResponse response =  new TarjetaResponse();
		User user = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); 
		String expira = "";
		String anio = "";
		String mes = "";
		try{
			
			LOGGER.debug("Añadiendo tarjeta MobileCard {}", carRequest);
			
			user= mapper.getUser(carRequest.getIdUsuario());
			user.setUsr_direccion(carRequest.getDireccion());
			user.setUsr_ciudad(carRequest.getCiudad());
			user.setUsr_cp(carRequest.getCp());
			user.setUsr_fecha_nac(new java.sql.Date(sdf.parse(carRequest.getFechaNac()).getTime()));
			user.setUsr_nss(carRequest.getSsn());
			
			//Crear Tarjeta FV
			CreateCardResponse responseFV = CreateCardFV(user.getUsr_nombre(),user.getUsr_apellido(),carRequest.getDireccion(),carRequest.getEstado(),carRequest.getCiudad(),carRequest.getCp(), user.geteMail(),user.getUsr_telefono(),carRequest.getFechaNac(),carRequest.getSsn(),carRequest.getIdUsuario());
			
			if(responseFV.getErrorCode() == 0 && responseFV.getCardNumber()!=null && responseFV.getCvv()!= null && responseFV.getExpirationDate() != null){
				
				 mes = responseFV.getExpirationDate().substring(0,2);
				 anio = responseFV.getExpirationDate().substring(responseFV.getExpirationDate().length()-2);
				 expira = mes+anio; //MMYY

				
				//Enrrolar con Ingo	
				CustomerResponse responseIngo = EnrollCustomer(carRequest.getDireccion(), carRequest.getCiudad(),carRequest.getFechaNac().substring(4,8)+"-"+carRequest.getFechaNac().substring(2,4)+"-"+carRequest.getFechaNac().substring(0,2),user.geteMail(), user.getUsr_nombre(), user.getUsr_apellido(), user.getUsr_telefono(), carRequest.getSsn(), carRequest.getEstado(),carRequest.getCp(),user.getImei());
				
				if(responseIngo.getErrorCode() == 0){
					
					user.setId_ingo(responseIngo.getCustomerId());
			        mapper.updateCustomerId(user);
			        
					//agregar targeta Ingo
			        CardsResponse responseCardIngo = addCardIngo(user.getUsr_direccion(), user.getUsr_nombre(), responseFV.getCardNumber(), carRequest.getCiudad(), responseIngo.getCustomerId(), user.getImei(),expira , (user.getUsr_nombre() + " " + user.getUsr_apellido()).trim() , carRequest.getEstado(), user.getUsr_cp());
					
					if(responseCardIngo.getErrorCode() == 0){
						LOGGER.debug("Se Agrego correctamente la tarjeta al usuario {}",responseIngo.getCustomerId() );
					}
					else
					{
						LOGGER.debug("Error al agregar tarjeta al usuario {} message {} ", responseIngo.getCustomerId(), responseCardIngo.getErrorMessage());
						response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
					}
				}
				else
				{
					LOGGER.debug("Error al enrrolar usuario en ingo {} {}", carRequest.getIdUsuario(), responseIngo.getErrorMessage());
					response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
				}
				
				//agregar Tarjeta al wallet
				TarjetaRequest cardRequest = new TarjetaRequest();
				cardRequest.setCodigo(responseFV.getCvv());
				cardRequest.setCpAmex("");
				cardRequest.setDeterminada(false);
				cardRequest.setDomAmex("");
				cardRequest.setIdioma(carRequest.getIdioma());
				cardRequest.setIdTarjeta(0);
				cardRequest.setIdUsuario(user.getId_usuario());
				cardRequest.setMobilecard(true);
				cardRequest.setNombre(user.getUsr_nombre().toString() + " " + user.getUsr_apellido().trim());
				cardRequest.setPan(AddcelCrypto.encryptHard(responseFV.getCardNumber()));
				cardRequest.setTipo(0);
				cardRequest.setVigencia(AddcelCrypto.encryptHard(mes+"/"+anio));
				cardRequest.setCodigo(AddcelCrypto.encryptHard(responseFV.getCvv()));
				
				response = addCard(cardRequest);
				
				if(response.getIdError() == 0){
					LOGGER.debug("Se agrego la Tarjeta correctamente al Wallet");
				}
				else{
					LOGGER.debug("Error al agregar tarejta al wallet {} {}", response.getMensajeError(), cardRequest.getIdUsuario());
					response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
				}
				
			}
			else{
				LOGGER.debug("Error al crear Targeta FirstView {} {} " + carRequest.getIdUsuario(), responseFV.getErrorDescription());
				response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2); //LogService.ErrorFV(carRequest.getIdioma().equals("es") ? 1 : 2);
			}
			
			
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al crear tarjetaMobileCard", ex);
			return LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
		}
		
	}*/
	
	private CustomerResponse EnrollCustomer(String address, String city, String dateOfBirth, String email, String firstName, String lastName, String mobileNumber, String ssn, String state, String zip, String deviceId){
		
		EnrollCustomerMC request = new EnrollCustomerMC();
		//EnrollCustomerRequest request = new EnrollCustomerRequest();
		CustomerResponse response = null;
			
		request.setAddressLine1(address);
		request.setAddressLine2("");
		request.setAllowTexts(true);
		request.setCity(city);
		request.setCountryOfOrigin(""); //optional 
		request.setDateOfBirth(dateOfBirth); //yyyy-MM-dd (e.g., 1978-09-18) 1994-07-02 00:00:00 
		request.setEmail(email);
		request.setFirstName(firstName);
		request.setGender(""); //Optional Single character string indicating M/F.
		request.setHomeNumber("");
		request.setLastName(lastName);
		request.setMiddleInitial(""); //Optional Single character middle initial. Max length is 1 character.
		request.setMobileNumber(mobileNumber);
		request.setSsn(ssn);
		request.setState(state);
		request.setSuffix(""); // optional Typically would contain one of Jr, Sr, etc.
		request.setTitle(""); // optional Typically would contain one of Mrs, Mr, etc.
		request.setZip(zip);
		request.setDeviceId(deviceId);
		
		
		response =  mcIngorequest.EnrollCustomer(request, deviceId);
		
		return response;
	}
	
	private CardsResponse addCardIngo(String address, String cardNickname, String cardNumber, String city, String custumerId, String deviceId, String expirationMonthYear, String nameOncard, String state, String zip){
		
		
		
		//AddOrUpdateCardRequest  request = new AddOrUpdateCardRequestMC();
		cardNickname = cardNickname.substring(0,cardNickname.length()> 17 ? 17 : cardNickname.length());
		AddOrUpdateCardRequestMC request = new AddOrUpdateCardRequestMC();
		request.setAddressLine1(address);
		request.setAddressLine2("");
		request.setCardNickname(cardNickname+"-MC");//
		request.setCardNumber(cardNumber);
		request.setCity(city);
		request.setCustomerId(custumerId);
		request.setDiviceId(deviceId);
		request.setExpirationMonthYear(expirationMonthYear);
		request.setNameOnCard( nameOncard );
		request.setState(state);
		request.setZip(zip);
		
		LOGGER.debug("request AddCard INGO: " + gson.toJson(request));
		CardsResponse response = mcIngorequest.AddOrUpdateCard(request, deviceId);
		
		return response;
		
	}
	
	public CreateCardResponse CreateCardFV(String name, String patern, String address, String state, String city, String cp, String email, String phone, String birthday, String ssn, long idUser, String govtId, String govtIdExpirationDate,String govtIdIssueDate,String govtIdIssueState){
		ServicesProxy proxy = new ServicesProxy();
		CreateCardResponse responseFV = null; 
		String json = "";
		
		String jsonResponseFV;
		try {
			

			CreateCard requestFV = new CreateCard();
			requestFV.setName(name);
			requestFV.setPatern(patern);
			requestFV.setAddress(address);
			requestFV.setState(state);
			requestFV.setCity(city);
			requestFV.setPostalCode(cp);
			requestFV.setEmail(email);
			requestFV.setPhone(phone);
			requestFV.setBirthday(birthday);
			requestFV.setSsn(ssn);
			requestFV.setIdUsuario(idUser);
			requestFV.setGovtId(govtId);
			requestFV.setGovtIdExpirationDate(govtIdExpirationDate);
			requestFV.setGovtIdIssueDate(govtIdIssueDate);
			requestFV.setGovtIdIssueState(govtIdIssueState);
			
			
			json = gson.toJson(requestFV);
			LOGGER.debug("json Request: " +  json);
			
			jsonResponseFV = proxy.createCard(json);
			LOGGER.debug("Response FV: " + jsonResponseFV);
			responseFV = gson.fromJson(jsonResponseFV, CreateCardResponse.class);
			
		} catch (RemoteException e) {
			LOGGER.error("Error al crear Tarjeta FirstView", e);
			e.printStackTrace();
		}
		
		return responseFV;
		
	}
	
	
	private McResponse active(HashMap parameters){
		String responseJson = mapper.ProcessCard(parameters);
		McResponse activa = gson.fromJson(responseJson, McResponse.class);
		return activa;
	}
	
	private TarjetaResponse getCards(HashMap parameters){
		TarjetaResponse response = new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("GetTResponse: " + responseJson);
			response = gson.fromJson("{\"TelecomCardCard\":"+ responseJson + "}", TarjetaResponse.class);
			LOGGER.info("response: " + responseJson);			
			HardCardDencrypt(response);
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas: " +  ex.getMessage());
			response.setIdError(103);
			response.setMensajeError("Error al obtener tarjetas desde base de datos");
			return response;
		}
		
	}
	
	private void HardCardDencrypt(TarjetaResponse response){
		int tam =  response.getTarjetas().size();
		String pan;
		String vig;
		String cod;
		for(int i=0; i<tam; i++)
		{
			pan = UtilsService.getSMS(response.getTarjetas().get(i).getPan());
			vig = UtilsService.getSMS(response.getTarjetas().get(i).getVigencia());
			
			response.getTarjetas().get(i).setPan(AddcelCrypto.encryptHard(pan));
			response.getTarjetas().get(i).setVigencia(AddcelCrypto.encryptHard(vig));
			
			if(response.getTarjetas().get(i).getCodigo() != null && !response.getTarjetas().get(i).getCodigo().isEmpty())
			{
				cod =  UtilsService.getSMS(response.getTarjetas().get(i).getCodigo());
				response.getTarjetas().get(i).setCodigo(AddcelCrypto.encryptHard(cod));
			}
			else
			{
				response.getTarjetas().get(i).setCodigo("");
			}
			
			
		}
		
	}
	
	
	private TarjetaResponse delete(HashMap parameters){
		TarjetaResponse response =  new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			McResponse delete = gson.fromJson(responseJson, McResponse.class);
			response.setIdError(delete.getIdError());
			response.setMensajeError(delete.getMensajeError());
			LOGGER.debug("idError: " + response.getIdError() );
			LOGGER.debug("Mensage: " +  response.getMensajeError());
			return response;
		}catch(Exception ex){
			LOGGER.error("error al enviar proceso borrar tarjeta en base de datos");
			response.setIdError(104);
			response.setMensajeError("Error al procesar borrado de tarjeta");
			return response;
		}
		
	}
	

	/*PRUEBAS PROBAR LOS WEB SERVICES DE INGO*/
	
	
	
	
	private CardsResponse GetCardIngo(String deviceId,String customerId){
		
		
		
		//AddOrUpdateCardRequest  request = new AddOrUpdateCardRequestMC();		
		
		GetRegisteredCardsRequestMC request = new GetRegisteredCardsRequestMC();
		request.setDeviceId(deviceId);
		request.setCustomerId(customerId);		
		CardsResponse response = mcIngorequest.GetRegisteredCards(request, deviceId);		
		LOGGER.debug("request AddCard: " + gson.toJson(request));
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	
	
	public TarjetaResponse GetCardI(String deviceId,String customerId){
		
		CardsResponse responseCardIngo = GetCardIngo(deviceId,customerId);
		TarjetaResponse response = new TarjetaResponse();
		
		if(responseCardIngo.getErrorCode() == 0)	{
			LOGGER.debug("Se consulto de manera correcta {}",responseCardIngo.getErrorCode() );
		}
		response.setIdError(responseCardIngo.getErrorCode());
		response.setMensajeError(responseCardIngo.getErrorMessage());		
		return response;
		
	}
	
	public TarjetaResponse GetSessionLoadI(String deviceId,String user, String pass){			
		
		String passdecrypt = AddcelCrypto.decryptHard("7aO1Rfve99pVnLYBO9cduA==");
		LOGGER.info("passdecrypt: "+passdecrypt);
		String passmx = generaSemillaAux(passdecrypt);
		LOGGER.info("passmx: "+passmx);
		String passwordReal = (Crypto.aesEncrypt(passmx, passdecrypt));
		LOGGER.info("passwordReal: "+passwordReal);
		return null;
		
		
//		SessionResponse responseSessionIngo = GetSessionLoad(deviceId,user,pass);		
//		
//		TarjetaResponse response = new TarjetaResponse();
//		
//		if (responseSessionIngo.getErrorCode() == 0)	{
//			LOGGER.debug("Se consulto de manera correcta {}",responseSessionIngo.getErrorCode() );
//		}
//		
//		response.setIdError(responseSessionIngo.getErrorCode());
//		response.setMensajeError(responseSessionIngo.getErrorMessage());		
//		return response;
		
	}
	
	public SessionResponse GetSessionLoad(String deviceId,String user, String pass){
		
		
		LoginRequest request = new LoginRequest();
		
		request.setDeviceId(deviceId);
		request.setUser(user);
		request.setPass(pass);		
		SessionResponse response = mcIngorequest.getSessionData(request, deviceId);				
		LOGGER.debug("request AddCard: " + gson.toJson(request));
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	
	
	public TarjetaResponse findCustomerI(String deviceId,String dateOfBirth, String ssn){			
		
		CustomerResponse responseFindCustomerIngo = findCustomer(deviceId,dateOfBirth,ssn);		
		
		TarjetaResponse response = new TarjetaResponse();
		
		if (responseFindCustomerIngo.getErrorCode() == 0)	{
			LOGGER.debug("Se consulto de manera correcta {}",responseFindCustomerIngo.getErrorCode() );
		}
		
		response.setIdError(responseFindCustomerIngo.getErrorCode());
		response.setMensajeError(responseFindCustomerIngo.getErrorMessage());		
		return response;
		
	}
	
	public CustomerResponse findCustomer(String deviceId,String dateOfBirth, String ssn){
		
		
		FindCustomerRequestMC request = new FindCustomerRequestMC();

		request.setDeviceId(deviceId);
		request.setDateOfBirth(dateOfBirth);
		request.setSsn(ssn);				
		CustomerResponse response =  mcIngorequest.FindCustomer(request, deviceId);
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	public TarjetaResponse enrollCustomerI(String address, String city, String dateOfBirth, String email, String firstName, String lastName, String mobileNumber, String ssn, String state, String zip, String deviceId){			
		
		CustomerResponse responseIngo = new CustomerResponse();		
		
		responseIngo =  EnrollCustomer(address, city, dateOfBirth, email, firstName, lastName, mobileNumber, ssn, state, zip, deviceId);
		
		TarjetaResponse response = new TarjetaResponse();
		
		if (responseIngo.getErrorCode() == 0)	{
			LOGGER.debug("Se consulto de manera correcta {}",responseIngo.getErrorCode() );
		}
		
		LOGGER.debug("responseIngo: " + gson.toJson(responseIngo));
		response.setIdError(responseIngo.getErrorCode());
		response.setMensajeError(responseIngo.getErrorMessage());
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}

	
	public TarjetaResponse addCardIngoI(String address, String cardNickname, String cardNumber, String city, String custumerId, String deviceId, String expirationMonthYear, String nameOncard, String state, String zip){			
		
		CardsResponse responseCardIngo = new CardsResponse();		
		
		responseCardIngo =  addCardIngo(address, cardNickname, cardNumber, city, custumerId, deviceId, expirationMonthYear, nameOncard, state, zip);
		
		
		TarjetaResponse response = new TarjetaResponse();
		
		if (responseCardIngo.getErrorCode() == 0)	{
			LOGGER.debug("Se consulto de manera correcta {}",responseCardIngo.getErrorCode() );
		}
		
		LOGGER.debug("responseCardIngo: " + gson.toJson(responseCardIngo));
		response.setIdError(responseCardIngo.getErrorCode());
		response.setMensajeError(responseCardIngo.getErrorMessage());
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	
	public CardsResponse ingoNotifications(String deviceId,String customerId, String customerEmailAddress, String firstName, String cancelledByDescription, String cardBinNumber, String cardId, String cardNickName, String checkSubmissionDate, String declineCode, String declineReason,String fundingDestinationIdentifier,String fundsAvailableDate,String lastFourOfCard,String lastName,String notificationId,String notificationTypeDescription,String promoCode,String promoType,String transactionId,String transactionStatusDescription,String transactionTypeDescription,int cancelledBy,Long fee,Long feeDeltaInCents,boolean isPpg,Long keyedAmount,Long loadAmount,Long notificationType,Long promoAmount,int transactionStatus,int transactionType){
		
		
		LOGGER.info("ingoNotifications");
		
		IngoNotifications request = new IngoNotifications();

		request.setCustomerId(customerId);
		request.setCustomerEmailAddress(customerEmailAddress);
		request.setFirstName(firstName);
		request.setCancelledByDescription(cancelledByDescription);
		request.setCardBinNumber(cardBinNumber);
		request.setCardId(cardId);
		request.setCardNickName(cardNickName);
		request.setCheckSubmissionDate(checkSubmissionDate);
		request.setDeclineCode(declineCode);
		request.setDeclineReason(declineReason);
		request.setFundingDestinationIdentifier(fundingDestinationIdentifier);
		request.setFundsAvailableDate(fundsAvailableDate);
		request.setLastFourOfCard(lastFourOfCard);
		request.setLastName(lastName);
		request.setNotificationId(notificationId);
		request.setNotificationTypeDescription(notificationTypeDescription);
		request.setPromoCode(promoCode);
		request.setPromoType(promoType);
		request.setTransactionId(transactionId);
		request.setTransactionStatusDescription(transactionStatusDescription);
		request.setTransactionTypeDescription(transactionTypeDescription);
		request.setCancelledBy(cancelledBy);
		request.setFee(fee);
		request.setFeeDeltaInCents(feeDeltaInCents);
		request.setIsPpg(isPpg);
		request.setKeyedAmount(keyedAmount);
		request.setLoadAmount(loadAmount);
		request.setNotificationType(notificationType);
		request.setPromoAmount(promoAmount);
		request.setTransactionStatus(transactionStatus);
		request.setTransactionType(transactionType);
			
		mcIngorequest.IngoNotifications(request, deviceId);
//		LOGGER.debug("response: " + gson.toJson(response));
		return null;
		
	}

	
	public TarjetaResponse ingoNotificationsI(String deviceId,String customerId, String customerEmailAddress, String firstName, String cancelledByDescription, String cardBinNumber, String cardId, String cardNickName, String checkSubmissionDate, String declineCode, String declineReason,String fundingDestinationIdentifier,String fundsAvailableDate,String lastFourOfCard,String lastName,String notificationId,String notificationTypeDescription,String promoCode,String promoType,String transactionId,String transactionStatusDescription,String transactionTypeDescription,int cancelledBy,Long fee,Long feeDeltaInCents,boolean isPpg,Long keyedAmount,Long loadAmount,Long notificationType,Long promoAmount,int transactionStatus,int transactionType){
		
//	public TarjetaResponse ingoNotificationsI(String deviceId,String customerId, String customerEmailAddress, String firstName, String cancelledByDescription, String cardBinNumber, String cardId, String cardNickName, String checkSubmissionDate, String declineCode, String declineReason,String fundingDestinationIdentifier,String fundsAvailableDate,String lastFourOfCard,String lastName,String notificationId,String notificationTypeDescription,String promoCode,String promoType,String transactionId,String transactionStatusDescription,String transactionTypeDescription,int cancelledBy,Long fee){
		
		LOGGER.info("ingoNotificationsI");
		CardsResponse responseCardIngo = new CardsResponse();		
		
		responseCardIngo =  ingoNotifications(deviceId,customerId,customerEmailAddress,firstName,cancelledByDescription,cardBinNumber,cardId,cardNickName,checkSubmissionDate,declineCode,declineReason,fundingDestinationIdentifier,fundsAvailableDate,lastFourOfCard,lastName,notificationId,notificationTypeDescription,promoCode,promoType,transactionId,transactionStatusDescription,transactionTypeDescription,cancelledBy,fee,feeDeltaInCents,isPpg,keyedAmount,loadAmount,notificationType,promoAmount,transactionStatus,transactionType);
		
		
		TarjetaResponse response = new TarjetaResponse();		
		
//		if (responseCardIngo.getErrorCode() == 0)	{
//			LOGGER.debug("Se consulto de manera correcta {}",responseCardIngo.getErrorCode() );
//		}
//		
//		LOGGER.debug("responseCardIngo: " + gson.toJson(responseCardIngo));
//		response.setIdError(responseCardIngo.getErrorCode());
//		response.setMensajeError(responseCardIngo.getErrorMessage());
//		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	
	public TarjetaResponse addCardTelecomIngoI(String address, String cardNickname, String cardNumber, String city, String custumerId, String deviceId, String expirationMonthYear, String nameOncard, String state, String zip){			
		
		CardsResponse responseCardIngo = new CardsResponse();		
		
		responseCardIngo =  addCardIngo(address, cardNickname, cardNumber, city, custumerId, deviceId, expirationMonthYear, nameOncard, state, zip);
		
		
		TarjetaResponse response = new TarjetaResponse();
		
		if (responseCardIngo.getErrorCode() == 0)	{
			LOGGER.debug("Se consulto de manera correcta {}",responseCardIngo.getErrorCode() );
		}
		
		LOGGER.debug("responseCardIngo: " + gson.toJson(responseCardIngo));
		response.setIdError(responseCardIngo.getErrorCode());
		response.setMensajeError(responseCardIngo.getErrorMessage());
		LOGGER.debug("response: " + gson.toJson(response));
		return response;
		
	}
	
	 public static String generaSemillaAux(String pass) {
		    int len = pass.length();
		    String key = "";

		    int carry;
		    for (carry = 0; carry < 32 / len; ++carry) {
		      key = key + pass;
		    }

		    for (carry = 0; key.length() < 32; ++carry) {
		      key = key + pass.charAt(carry);
		    }

		    return key;
		  }


	/*FIN DE LAS PRUEBAS DE INGO*/

}
