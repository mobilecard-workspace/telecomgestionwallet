package com.addcel.telecom.wallet.services;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.ServicesEndPoint;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.IngoNotifications;
import com.google.gson.Gson;

@Service
public class MCIngorequest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MCIngorequest.class);
	
	@Autowired
	private RestTemplate restTemplate;
	private Gson gson = new Gson();
	
public AuthenticatePartnerResponse getSession(SessionRequest session, String deviceId ){
		
		try{
			
			HttpHeaders headers = getHeader(deviceId, null);
			HttpEntity<SessionRequest> request = new HttpEntity<SessionRequest>(session,headers);

			AuthenticatePartnerResponse responseObject  =  (AuthenticatePartnerResponse)  restTemplate.postForObject( ServicesEndPoint.getSession, request, AuthenticatePartnerResponse.class);
			return responseObject;
		
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}

public AuthenticateOBOResponse AuthenticateOBO (AuthenticateOBORequest authenticateOBORequest, String deviceId){
	try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<AuthenticateOBORequest> request = new HttpEntity<AuthenticateOBORequest>(authenticateOBORequest, headers);
		
		AuthenticateOBOResponse responseObject  =  (AuthenticateOBOResponse)  restTemplate.postForObject(ServicesEndPoint.AuthenticateOBO , request, AuthenticateOBOResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CustomerResponse EnrollCustomer(EnrollCustomerMC enrollCustomerRequest, String deviceId){
	
try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<EnrollCustomerRequest> request = new HttpEntity<EnrollCustomerRequest>(enrollCustomerRequest, headers);
		
		CustomerResponse responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.EnrollCustomer , request, CustomerResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CardsResponse AddOrUpdateCard(AddOrUpdateCardRequestMC addOrUpdateCardRequest, String deviceId){
	
try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<AddOrUpdateCardRequest> request = new HttpEntity<AddOrUpdateCardRequest>(addOrUpdateCardRequest, headers);
		
		CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.AddOrUpdateCard , request, CardsResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CustomerResponse FindCustomer(FindCustomerRequest Findrequest, String deviceId){
	try{
		HttpHeaders headers = getHeader(deviceId, null);
		HttpEntity<FindCustomerRequest> request = new HttpEntity<FindCustomerRequest>(Findrequest, headers);
		CustomerResponse responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.FindCustomer , request, CustomerResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CardsResponse GetRegisteredCards(GetRegisteredCardsRequestMC Getrequest, String deviceId){
	try{
		HttpHeaders headers = getHeader(deviceId, null);
		HttpEntity<GetRegisteredCardsRequestMC> request = new HttpEntity<GetRegisteredCardsRequestMC>(Getrequest, headers);
		LOGGER.debug("request GetRegisteredCards: " + gson.toJson(request));
		CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.GetRegisteredCards , request, CardsResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		LOGGER.error("Error al obtener tarjeta registradas", ex);
		return null;
	}
}


public CardsResponse IngoNotifications(IngoNotifications Getrequest, String deviceId){
	try{
		HttpHeaders headers = getHeader(deviceId, null);
		HttpEntity<IngoNotifications> request = new HttpEntity<IngoNotifications>(Getrequest, headers);
		LOGGER.debug("request GetRegisteredCards: " + gson.toJson(request));
		CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.IngoNotifications , request, CardsResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		LOGGER.error("Error al obtener tarjeta registradas", ex);
		return null;
	}
}

private HttpHeaders getHeader(String deviceId, String  sessionId){
	
	try{
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
			
		if(deviceId != null)
			headers.add("deviceId", deviceId);
		
		if(sessionId != null)
			headers.add("sessionId", sessionId);
		
		String credentials = "guest:guest";
		headers.add("Authorization", "Basic " + new String(Base64.getEncoder().encode(credentials.getBytes())));
		
		return headers;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
		
}

public SessionResponse getSessionData(LoginRequest loginRequest, String deviceId){
	
	try{
		HttpHeaders headers = getHeader(deviceId, null);				   
		HttpEntity<LoginRequest> request = new HttpEntity<LoginRequest>(loginRequest, headers);
		LOGGER.debug("request : " + gson.toJson(request));
		SessionResponse responseObject  =  (SessionResponse)  restTemplate.postForObject(ServicesEndPoint.getSessionData , request, SessionResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		LOGGER.error("Error al obtener la sesion Data", ex);
		return null;
	}
}	


}



