package com.addcel.telecom.wallet.mybatis.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.wallet.client.model.vo.TelecomCard;
import com.addcel.telecom.wallet.mybatis.model.vo.Card;
import com.addcel.telecom.wallet.mybatis.model.vo.Estados;
import com.addcel.telecom.wallet.mybatis.model.vo.Login;
import com.addcel.telecom.wallet.mybatis.model.vo.User;

//import com.addcel.ingo.mcingo.mybatis.model.vo.ClienteVO;
//import com.addcel.ingo.mcingo.mybatis.model.vo.DataUser;
//import com.addcel.ingo.mcingo.mybatis.model.vo.User;



public interface ServiceMapper {

	public String ProcessCard(HashMap parameters);
	public Login getLogin(long id_usuario);
	public List<Estados> getEstates(int idpais);
	public User getUser(long id_usuario);
	public void updateCustomerId(User user);
	public Card getTelecomcardCard(long id_usuario);
	
	//public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
//	public String getCustumerID(@Param(value = "user") String user, @Param(value = "pass") String pass);
	
	//public String  getUser(HashMap datauser);
	//public void updateCustomerId(User user);
	
}
