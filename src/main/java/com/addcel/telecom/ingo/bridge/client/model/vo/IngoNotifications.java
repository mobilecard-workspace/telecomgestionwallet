package com.addcel.telecom.ingo.bridge.client.model.vo;

//import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IngoNotifications {

	@JsonProperty("FirstName")
	private String FirstName;
	@JsonProperty("LastName")
	private String LastName;
	@JsonProperty("CustomerEmailAddress")
	private String CustomerEmailAddress;
	@JsonProperty("CustomerId")
	private String CustomerId;
	@JsonProperty("TransactionId")
	private String TransactionId;
	@JsonProperty("KeyedAmount")
	private Long KeyedAmount;
	@JsonProperty("LoadAmount")
	private Long LoadAmount;
	@JsonProperty("Fee")
	private Long Fee;
	@JsonProperty("FeeDeltaInCents")
	private Long FeeDeltaInCents;
	@JsonProperty("IsPpg")
	private Boolean IsPpg;
	@JsonProperty("PromoCode")
	private String PromoCode;
	@JsonProperty("PromoType")
	private String PromoType;
	@JsonProperty("PromoAmount")
	private Long PromoAmount;
	@JsonProperty("CardId")
	private String CardId;
	@JsonProperty("CardNickName")
	private String CardNickName;
	@JsonProperty("LastFourOfCard")
	private String LastFourOfCard;
	@JsonProperty("CardBinNumber")
	private String CardBinNumber;
	@JsonProperty("FundingDestinationIdentifier")
	private String FundingDestinationIdentifier;
	@JsonProperty("CheckSubmissionDate")
	private String CheckSubmissionDate;
	@JsonProperty("FundsAvailableDate")
	private String FundsAvailableDate;
	@JsonProperty("NotificationId")
	private String NotificationId;
	@JsonProperty("NotificationType")
	private Long NotificationType;
	@JsonProperty("NotificationTypeDescription")
	private String NotificationTypeDescription;
	@JsonProperty("TransactionType")
	private Integer TransactionType;
	@JsonProperty("TransactionTypeDescription")
	private String TransactionTypeDescription;
	@JsonProperty("TransactionStatus")
	private Integer TransactionStatus;
	@JsonProperty("TransactionStatusDescription")
	private String TransactionStatusDescription;
	@JsonProperty("CancelledBy")
	private Integer CancelledBy;
	@JsonProperty("CancelledByDescription")
	private String CancelledByDescription;
	@JsonProperty("DeclineReason")
	private String DeclineReason;
	@JsonProperty("DeclineCode")
	private String DeclineCode;
	
	
	
	public IngoNotifications() {
		// TODO Auto-generated constructor stub
	}
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getCustomerEmailAddress() {
		return CustomerEmailAddress;
	}
	public void setCustomerEmailAddress(String customerEmailAddress) {
		CustomerEmailAddress = customerEmailAddress;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getTransactionId() {
		return TransactionId;
	}
	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}
	public Long getKeyedAmount() {
		return KeyedAmount;
	}
	public void setKeyedAmount(Long keyedAmount) {
		KeyedAmount = keyedAmount;
	}
	public Long getLoadAmount() {
		return LoadAmount;
	}
	public void setLoadAmount(Long loadAmount) {
		LoadAmount = loadAmount;
	}
	public Long getFee() {
		return Fee;
	}
	public void setFee(Long fee) {
		Fee = fee;
	}
	public Long getFeeDeltaInCents() {
		return FeeDeltaInCents;
	}
	public void setFeeDeltaInCents(Long feeDeltaInCents) {
		FeeDeltaInCents = feeDeltaInCents;
	}
	public Boolean getIsPpg() {
		return IsPpg;
	}
	public void setIsPpg(Boolean isPpg) {
		IsPpg = isPpg;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}
	public String getPromoType() {
		return PromoType;
	}
	public void setPromoType(String promoType) {
		PromoType = promoType;
	}
	public Long getPromoAmount() {
		return PromoAmount;
	}
	public void setPromoAmount(Long promoAmount) {
		PromoAmount = promoAmount;
	}
	public String getCardId() {
		return CardId;
	}
	public void setCardId(String cardId) {
		CardId = cardId;
	}
	public String getCardNickName() {
		return CardNickName;
	}
	public void setCardNickName(String cardNickName) {
		CardNickName = cardNickName;
	}
	public String getLastFourOfCard() {
		return LastFourOfCard;
	}
	public void setLastFourOfCard(String lastFourOfCard) {
		LastFourOfCard = lastFourOfCard;
	}
	public String getCardBinNumber() {
		return CardBinNumber;
	}
	public void setCardBinNumber(String cardBinNumber) {
		CardBinNumber = cardBinNumber;
	}
	public String getFundingDestinationIdentifier() {
		return FundingDestinationIdentifier;
	}
	public void setFundingDestinationIdentifier(String fundingDestinationIdentifier) {
		FundingDestinationIdentifier = fundingDestinationIdentifier;
	}
	public String getCheckSubmissionDate() {
		return CheckSubmissionDate;
	}
	public void setCheckSubmissionDate(String checkSubmissionDate) {
		CheckSubmissionDate = checkSubmissionDate;
	}
	public String getFundsAvailableDate() {
		return FundsAvailableDate;
	}
	public void setFundsAvailableDate(String fundsAvailableDate) {
		FundsAvailableDate = fundsAvailableDate;
	}
	public String getNotificationId() {
		return NotificationId;
	}
	public void setNotificationId(String notificationId) {
		NotificationId = notificationId;
	}
	public Long getNotificationType() {
		return NotificationType;
	}
	public void setNotificationType(Long notificationType) {
		NotificationType = notificationType;
	}
	public String getNotificationTypeDescription() {
		return NotificationTypeDescription;
	}
	public void setNotificationTypeDescription(String notificationTypeDescription) {
		NotificationTypeDescription = notificationTypeDescription;
	}
	public Integer getTransactionType() {
		return TransactionType;
	}
	public void setTransactionType(Integer transactionType) {
		TransactionType = transactionType;
	}
	public String getTransactionTypeDescription() {
		return TransactionTypeDescription;
	}
	public void setTransactionTypeDescription(String transactionTypeDescription) {
		TransactionTypeDescription = transactionTypeDescription;
	}
	public Integer getTransactionStatus() {
		return TransactionStatus;
	}
	public void setTransactionStatus(Integer transactionStatus) {
		TransactionStatus = transactionStatus;
	}
	public String getTransactionStatusDescription() {
		return TransactionStatusDescription;
	}
	public void setTransactionStatusDescription(String transactionStatusDescription) {
		TransactionStatusDescription = transactionStatusDescription;
	}
	public Integer getCancelledBy() {
		return CancelledBy;
	}
	public void setCancelledBy(Integer cancelledBy) {
		CancelledBy = cancelledBy;
	}
	public String getCancelledByDescription() {
		return CancelledByDescription;
	}
	public void setCancelledByDescription(String cancelledByDescription) {
		CancelledByDescription = cancelledByDescription;
	}
	public String getDeclineReason() {
		return DeclineReason;
	}
	public void setDeclineReason(String declineReason) {
		DeclineReason = declineReason;
	}
	public String getDeclineCode() {
		return DeclineCode;
	}
	public void setDeclineCode(String declineCode) {
		DeclineCode = declineCode;
	}
	
}

 