package com.addcel.telecom.ingo.bridge.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FindCustomerRequestMC extends FindCustomerRequest{

	private String deviceId;

	public FindCustomerRequestMC() {
		// TODO Auto-generated constructor stub
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
	
}
