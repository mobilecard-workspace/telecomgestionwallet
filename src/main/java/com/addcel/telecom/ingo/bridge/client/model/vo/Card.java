package com.addcel.telecom.ingo.bridge.client.model.vo;

public class Card {

	public String cardArtAsBase64Png;
	public String cardId;
	public String cardNickname;
	public String cardProgram;
	public String cardProgramPhone;
	public Integer currentCardMax;
	public Integer currentCardMin;
	public String customerId;
	public String expirationMonthYear;
	public String hashId;
	public Integer issuerType;
	public String lastFourDigits;
	public Boolean showTermsAndConditions;
	public String termsAndConditionsId;
	
	public String getCardArtAsBase64Png() {
		return cardArtAsBase64Png;
	}
	public void setCardArtAsBase64Png(String cardArtAsBase64Png) {
		this.cardArtAsBase64Png = cardArtAsBase64Png;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getCardNickname() {
		return cardNickname;
	}
	public void setCardNickname(String cardNickname) {
		this.cardNickname = cardNickname;
	}
	public String getCardProgram() {
		return cardProgram;
	}
	public void setCardProgram(String cardProgram) {
		this.cardProgram = cardProgram;
	}
	public String getCardProgramPhone() {
		return cardProgramPhone;
	}
	public void setCardProgramPhone(String cardProgramPhone) {
		this.cardProgramPhone = cardProgramPhone;
	}
	public Integer getCurrentCardMax() {
		return currentCardMax;
	}
	public void setCurrentCardMax(Integer currentCardMax) {
		this.currentCardMax = currentCardMax;
	}
	public Integer getCurrentCardMin() {
		return currentCardMin;
	}
	public void setCurrentCardMin(Integer currentCardMin) {
		this.currentCardMin = currentCardMin;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getExpirationMonthYear() {
		return expirationMonthYear;
	}
	public void setExpirationMonthYear(String expirationMonthYear) {
		this.expirationMonthYear = expirationMonthYear;
	}
	public String getHashId() {
		return hashId;
	}
	public void setHashId(String hashId) {
		this.hashId = hashId;
	}
	public Integer getIssuerType() {
		return issuerType;
	}
	public void setIssuerType(Integer issuerType) {
		this.issuerType = issuerType;
	}
	public String getLastFourDigits() {
		return lastFourDigits;
	}
	public void setLastFourDigits(String lastFourDigits) {
		this.lastFourDigits = lastFourDigits;
	}
	public Boolean getShowTermsAndConditions() {
		return showTermsAndConditions;
	}
	public void setShowTermsAndConditions(Boolean showTermsAndConditions) {
		this.showTermsAndConditions = showTermsAndConditions;
	}
	public String getTermsAndConditionsId() {
		return termsAndConditionsId;
	}
	public void setTermsAndConditionsId(String termsAndConditionsId) {
		this.termsAndConditionsId = termsAndConditionsId;
	}
	
	
}
