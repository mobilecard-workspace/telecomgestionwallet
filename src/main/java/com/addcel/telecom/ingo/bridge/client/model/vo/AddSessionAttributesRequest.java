package com.addcel.telecom.ingo.bridge.client.model.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddSessionAttributesRequest {

	private List<Attributes> sessionAttributes = new ArrayList<Attributes>();

	public AddSessionAttributesRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public List<Attributes> getSessionAttributes() {
		return sessionAttributes;
	}

	public void setSessionAttributes(List<Attributes> sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}
	
	
	
}
