package com.addcel.telecom.ingo.bridge.client.model.vo;

public class Account {

	private String accountId;
	private String accountNickname;
	private Address address;
	private String customerId;
	private AccountSpecificProperties accountSpecificProperties;
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountNickname() {
		return accountNickname;
	}
	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public AccountSpecificProperties getAccountSpecificProperties() {
		return accountSpecificProperties;
	}
	public void setAccountSpecificProperties(
			AccountSpecificProperties accountSpecificProperties) {
		this.accountSpecificProperties = accountSpecificProperties;
	}
	
	
}
