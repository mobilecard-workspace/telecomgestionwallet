package com.addcel.telecom.ingo.bridge.client.model.vo;

public class SearchResult {

	private String actualLoadDate;
	private Long amount;
	private String cardNickname;
	private Long classificationStatus;
	private String createdOn;
	private Long declineReasonCode;
	private String declineReasonMessage;
	private String destinationDisplayName;
	private String expectedLoadDate;
	private Long fee;
	private String finishedApprovalOn;
	private String finishedClassificationOn;
	private String lastFourDigitsOfCard;
	private Long loadAmount;
	private Long loadStatus;
	private Long mobileTransactionTypeId;
	private Long ocrAmount;
	private Long pickupLocationId;
	private Long processingStatus;
	private Long state;
	private Long statusCode;
	private String statusMessage;
	private String submittedForApprovalOn;
	private String submittedForClassificationOn;
	private String transactionId;
	private Long userEnteredAmount;
	
	public String getActualLoadDate() {
		return actualLoadDate;
	}
	public void setActualLoadDate(String actualLoadDate) {
		this.actualLoadDate = actualLoadDate;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getCardNickname() {
		return cardNickname;
	}
	public void setCardNickname(String cardNickname) {
		this.cardNickname = cardNickname;
	}
	public Long getClassificationStatus() {
		return classificationStatus;
	}
	public void setClassificationStatus(Long classificationStatus) {
		this.classificationStatus = classificationStatus;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Long getDeclineReasonCode() {
		return declineReasonCode;
	}
	public void setDeclineReasonCode(Long declineReasonCode) {
		this.declineReasonCode = declineReasonCode;
	}
	public String getDeclineReasonMessage() {
		return declineReasonMessage;
	}
	public void setDeclineReasonMessage(String declineReasonMessage) {
		this.declineReasonMessage = declineReasonMessage;
	}
	public String getDestinationDisplayName() {
		return destinationDisplayName;
	}
	public void setDestinationDisplayName(String destinationDisplayName) {
		this.destinationDisplayName = destinationDisplayName;
	}
	public String getExpectedLoadDate() {
		return expectedLoadDate;
	}
	public void setExpectedLoadDate(String expectedLoadDate) {
		this.expectedLoadDate = expectedLoadDate;
	}
	public Long getFee() {
		return fee;
	}
	public void setFee(Long fee) {
		this.fee = fee;
	}
	public String getFinishedApprovalOn() {
		return finishedApprovalOn;
	}
	public void setFinishedApprovalOn(String finishedApprovalOn) {
		this.finishedApprovalOn = finishedApprovalOn;
	}
	public String getFinishedClassificationOn() {
		return finishedClassificationOn;
	}
	public void setFinishedClassificationOn(String finishedClassificationOn) {
		this.finishedClassificationOn = finishedClassificationOn;
	}
	public String getLastFourDigitsOfCard() {
		return lastFourDigitsOfCard;
	}
	public void setLastFourDigitsOfCard(String lastFourDigitsOfCard) {
		this.lastFourDigitsOfCard = lastFourDigitsOfCard;
	}
	public Long getLoadAmount() {
		return loadAmount;
	}
	public void setLoadAmount(Long loadAmount) {
		this.loadAmount = loadAmount;
	}
	public Long getLoadStatus() {
		return loadStatus;
	}
	public void setLoadStatus(Long loadStatus) {
		this.loadStatus = loadStatus;
	}
	public Long getMobileTransactionTypeId() {
		return mobileTransactionTypeId;
	}
	public void setMobileTransactionTypeId(Long mobileTransactionTypeId) {
		this.mobileTransactionTypeId = mobileTransactionTypeId;
	}
	public Long getOcrAmount() {
		return ocrAmount;
	}
	public void setOcrAmount(Long ocrAmount) {
		this.ocrAmount = ocrAmount;
	}
	public Long getPickupLocationId() {
		return pickupLocationId;
	}
	public void setPickupLocationId(Long pickupLocationId) {
		this.pickupLocationId = pickupLocationId;
	}
	public Long getProcessingStatus() {
		return processingStatus;
	}
	public void setProcessingStatus(Long processingStatus) {
		this.processingStatus = processingStatus;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Long getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Long statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getSubmittedForApprovalOn() {
		return submittedForApprovalOn;
	}
	public void setSubmittedForApprovalOn(String submittedForApprovalOn) {
		this.submittedForApprovalOn = submittedForApprovalOn;
	}
	public String getSubmittedForClassificationOn() {
		return submittedForClassificationOn;
	}
	public void setSubmittedForClassificationOn(String submittedForClassificationOn) {
		this.submittedForClassificationOn = submittedForClassificationOn;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Long getUserEnteredAmount() {
		return userEnteredAmount;
	}
	public void setUserEnteredAmount(Long userEnteredAmount) {
		this.userEnteredAmount = userEnteredAmount;
	}
	
	
	
}
